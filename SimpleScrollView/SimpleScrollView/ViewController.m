//
//  ViewController.m
//  SimpleScrollView
//
//  Created by Matthew Griffin on 5/15/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    float width = self.view.frame.size.width;
    float height = self.view.frame.size.height;
    
    UIScrollView* Scroller = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    Scroller.pagingEnabled = YES;
    [Scroller setContentSize:CGSizeMake(width * 5, Scroller.frame.size.height)];
    [self.view addSubview:Scroller];
    
    for (int i = 0; i < 5; i++) {
        UIView* view = [[UIView alloc]initWithFrame:CGRectMake(i * self.view.frame.size.width, 0, 1, self.view.frame.size.height)];
        view.backgroundColor = [UIColor blackColor];
        [Scroller addSubview:view];
    };
    
    NSArray* imageArr = @[@"Macaron", @"Cookie", @"Smooth", @"cake", @"Soda"];
    
    
    
    for (int i = 0; i < 5; i++) {
        UIImageView* image = [[UIImageView alloc]initWithFrame:CGRectMake(20 + (i * width), (height/10) +20 , width - 40, height/2- 30)];
        image.contentMode = UIViewContentModeScaleAspectFit;
        [image setImage:[UIImage imageNamed:imageArr[i]]];
        
        
        [Scroller addSubview:image];
    };
    
    NSArray* Instructions = @[@"Line three large cookie sheets with parchment paper; set aside. In a medium bowl stir together almonds, powdered sugar, and ginger; set aside.\nIn a large bowl combine egg whites, vanilla, and salt. Beat with an electric mixer on medium speed until frothy. Gradually add granulated sugar, about 1 tablespoon at a time, beating on high speed just until soft peaks form (tips curl). Stir in nut mixture and food coloring.\nSpoon mixture into a large decorating bag fitted with a large (about 1/2-inch) round tip.* Pipe 1 1/2-inch circles 1 inch apart onto the prepared cookie sheets. Let stand for 30 minutes before baking.\nMeanwhile, preheat oven to 325 degrees F. Bake in the preheated oven for 9 to 10 minutes or until set. Cool on cookie sheets on wire racks. Carefully peel cookies off parchment paper.\nIn a small saucepan combine caramels and cream; heat and stir over low heat until melted and smooth. Spread caramel mixture on bottoms of half of the cookies; immediately after spreading, sprinkle each with a little coarse sea salt. Top with the remaining cookies, bottom sides down.", @"Cookies. Heat oven to 350 degrees F. Cover 2 baking sheets with nonstick aluminum foil; set aside.\nIn medium-size bowl, combine flour, baking soda and salt. In large bowl, beat together sugar and butter until smooth and creamy, about 1 minute. Beat in egg until well combined. On low speed, alternately beat in flour mixture and the buttermilk, beginning and ending with flour mixture\nDrop 1/4 cupfuls of dough onto prepared baking sheets, spacing 3 inches apart and spreading into large circles, about 4 inches across.\nBake at 350 degrees F for 13 minutes or until toothpick inserted in centers tests clean. Remove cookies to rack to cool.", @"In a blender combine bananas, strawberries or mango slices, grape juice or fruit nectar, yogurt, and, if desired, honey. Cover and blend until smooth. Pour into six tall, chilled glasses. If desired, sprinkle with ground pistachio nuts. Makes 6 smoothies.", @"Preheat the oven to 350 degrees . Line 24 mini muffin cups. In a large heat-proof bowl, combine chocolate and oil. Place bowl over a pot of simmering water and let stand until chocolate is melted. Stir until smooth; set aside.\nUsing an electric mixer set to medium speed, whip eggs and sugar until pale and thickened slightly, about 6 minutes. Using a rubber spatula, stir in chocolate mixture, vanilla extract, salt and flour until smooth. Spoon batter into prepared muffin cups, filling each three-quarters full. Bake until cakes are puffed up and set on top, 6 to 8 minutes (cakes will fall slightly as they cool).\nCool 5 minutes, then carefully remove cakes from muffin cups. Allow to cool completely, then dust with sifted confectioners' sugar.", @"Line twenty-four 2 1/2-inch muffin cups with paper bake cups; set aside. Prepare cake mix as directed on package. Fill muffin cups two-thirds full with batter. (Do not overfill; it will be easier to spoon the gelatin mixture over the cupcakes later if they are not domed.) Bake in a 350 degrees oven for 15 to 20 minutes or until a wooden toothpick inserted in centers comes out clean. Cool in cups on a wire rack for 10 minutes.\nMeanwhile, in a small bowl, dissolve half of one box of gelatin (about 3 tablespoons) in 1/4 cup of the boiling water. In a separate small bowl, repeat with another flavor of gelatin. Let stand for 5 minutes. (Reserve remaining gelatin from each box for frosting.) Slowly pour 1/4 cup carbonated beverage into each bowl; stir gently.\nWhile cupcakes are still in the pan, poke each cake a few times with a fork. Generously but carefully spoon or brush one of the warm gelatin mixtures over 12 of the cupcakes, and the other gelatin mixture over the remaining cupcakes. Repeat, if necessary, to ensure cupcakes are well saturated with the gelatin mixture. Discard remaining gelatin mixture. Chill cupcakes in pans about 3 hours or until set.\nFor frosting: In two small mixing bowls, dissolve each flavor of remaining gelatin in 1 tablespoon each milk. Set bowls aside. In a large mixing bowl, beat cream cheese and butter with an electric mixer on medium speed until light and fluffy. Gradually beat in powdered sugar to reach piping consistency. Remove about half the white frosting to a medium bowl; set aside. (That portion will remain white.) Divide the remaining frosting (in the large mixing bowl) between the two smaller bowls with the gelatin mixtures. Beat each with a spoon to tint the frosting. (In the end, you will have two small bowls of colored frosting and one medium bowl of white frosting.)\nFit two pastry bags with large open-star decorating tips. To fill each bag, spoon one colored frosting and half the white frosting into the bag, placing the frostings side by side (not one on top of the other). Pipe two-tone swirls onto the tops of cupcakes. Garnish each with a cut drinking straw. Cover; chill to store."];
    
    for(int i = 0; i < 5; i++)
    {
        UITextView*  textblock = [[UITextView alloc]initWithFrame:CGRectMake(20 + (i * width), (height/5)*4 , (width - 40), 100)];
        textblock.text = Instructions[i];
        textblock.backgroundColor =[UIColor orangeColor];
        textblock.textColor = [UIColor whiteColor];
        textblock.editable = NO;
        [Scroller addSubview:textblock];
    };
    
    NSArray* LabelArray = @[@"Salted Carmel Macaroons", @"Retro Black and White Cookies", @"Mixed-Fruit Smoothie", @"Chocolate Soufle Cakes", @"Soda Fountain Cupcakes"];
    
    for (int i = 0; i < 5; i++) {
        UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(20 + (i * width), width/10, width - 40, height/20)];
        lbl.textColor = [UIColor whiteColor];
        lbl.text = LabelArray[i];
        lbl.backgroundColor = [UIColor orangeColor];
        lbl.textAlignment = NSTextAlignmentCenter;
        
        [Scroller addSubview:lbl];
    };
    
    NSArray* Ingredients = @[@"1/2 cups finely ground almonds\n1/4 cups powdered sugar\n1/2 teaspoons ground ginger\n3 egg whites\n1/2 teaspoon vanilla\nDash salt\n1/4 cup granulated sugar\n6 yellow food coloring\n1/2 14 ounce package vanilla caramels, unwrapped\n2 tablespoons whipping cream\nCoarse sea salt", @"1/4 cups all-purpose flour\n1/2 teaspoon baking soda\n1/2 teaspoon salt\n1/2 cup granulated sugar\n1/3 cup unsalted butter, softened\n1 egg\n1/3 cup buttermilk", @"2 bananas, chilled\n2/3 cup strawberries or mango slices\n12 ounce can grape juice or mango, apricot, strawberry, or other fruit nectar, chilled\n8 ounce carton fat-free yogurt\ntablespoon honey (optional)\n2 tablespoons ground pistachio nuts (optional)", @"6 ounces semisweet chocolate, chopped\n1/4 cup vegetable oil\n3 large eggs\n6 tablespoons granulated sugar\n1/2 teaspoon vanilla extract\n1/4 teaspoon salt\n4 teaspoons all-purpose four\ntablespoon confectioners' sugar", @"two-layer size white cake mix\n2 3-ounce packages fruit-flavored gelatin (choose two flavors, such as lime, cherry, strawberry, grape or orange)\n1/2 cup boiling water, divided\n1/2 cup lemon-lime carbonated beverage, ginger ale or cold water; divided\n6 ounces cream cheese, softened\n1/2 cup butter, softened\n6 - 7 cups powdered sugar\n2 tablespoons milk, divided\nDrinking straws, cut into 3-inch pieces"];
    
    for(int i = 0; i < 5; i++)
    {
        UITextView*  textblock = [[UITextView alloc]initWithFrame:CGRectMake(20 + (i * width), ((height/4)*3) - 100 , (width - 40), 100)];
        textblock.text = Ingredients[i];
        textblock.backgroundColor =[UIColor purpleColor];
        textblock.textColor = [UIColor whiteColor];
        textblock.editable = NO;
        [Scroller addSubview:textblock];
    };
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
