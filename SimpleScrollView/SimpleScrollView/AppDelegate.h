//
//  AppDelegate.h
//  SimpleScrollView
//
//  Created by Matthew Griffin on 5/15/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

